#include <iostream>
#include <memory>

#include "expresscpp/expresscpp.hpp"

int main() {
  std::cout << "Hello World!" << std::endl;

  auto expresscpp = std::make_shared<ExpressCpp>();
  expresscpp->Get("/",
                  [](auto /*req*/, auto res) { res->Send("hello world!"); });
  expresscpp->Stack();
  constexpr uint16_t port = 8081u;
  expresscpp->Listen(port, []() {
    std::cout << "Example app listening on port " << port << std::endl;
  });
  return 0;
}