# Example project with ExpressCpp

## Build

```bash
# add conan package manager
pip3 install conan --upgrade
# add expresscpp remote to list of sources
# TODO: add expresscpp to conan github repo -> no need to add this anymore
conan remote add expresscpp https://api.bintray.com/conan/expresscpp/expresscpp/

mkdir build
cd build
cmake ..
make -j
```
